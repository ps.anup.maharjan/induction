#!/bin/bash

# Set the disk usage threshold
threshold=50

# Get the current disk usage for the root partition
usage=$(df -h / | awk '{ print $5 }' | tail -n 1 | sed 's/%//')

# Get the hostname of the system
hostname=$(hostname)

# Check if the usage is above the threshold
if [ $usage -gt $threshold ]; then
    # Send an email alert
    echo "Disk usage on $hostname is above the threshold ($threshold%): $usage%" | mail -s "Disk usage alert on $hostname" -S smtp=smtp.office365.com:    993 -S smtp-auth=login -S smtp-auth-user=Anup -S smtp-auth-password=Password anup.maharjan@clusus.com
    ~/cleanup.sh
    
fi

