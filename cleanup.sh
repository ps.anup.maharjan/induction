#!/bin/bash

# Delete all .log files older than 3 days in the logs directory
find ~/logs/*.log -mtime +3 -exec rm {} \;

